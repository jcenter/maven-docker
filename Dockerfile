FROM docker:dind

MAINTAINER wangkun23 <845885222@qq.com>


ENV HELM_VERSION=3.6.1
ENV HELM_URL="https://get.helm.sh"

RUN apk add --no-cache --update maven openjdk8

RUN wget ${HELM_URL}/helm-v${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xz && \
    mv linux-amd64/helm /usr/bin/helm && \
    chmod +x /usr/bin/helm && \
    rm -rf linux-amd64

RUN chmod +x /usr/bin/helm && rm -rf /var/cache/apk/*


    
